# README #

Run in the browser process,
Can be tested on pc

```
gles_test # clang source/egl.c sys_log.c -lGL -lEGL -ggdb

gles_test # ./a.out 

Preparing PigConfig Struct

Getting Struct values
00007ffd35e4f3c8  80 00 00 00 28 00 00 00  00 00 00 00 00 00 00 00  |....(...........|
00007ffd35e4f3d8  00 00 00 00 00 00 00 00  00 00 80 00 00 00 00 00  |................|
00007ffd35e4f3e8  00 00 00 00 00 00 00 00  00 00 40 0e 00 00 00 00  |..........@.....|
00007ffd35e4f3f8  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00007ffd35e4f408  80 07 00 00 38 04 00 00  00 00 00 00 00 00 00 00  |....8...........|
00007ffd35e4f418  00 00 00 00 00 00 02 00  00 40 00 00 02 00 00 00  |.........@......|
00007ffd35e4f428  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00007ffd35e4f438  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
eglDisplay      : 0x131b6b0
eglInitialize   : 1
eglVersionMajor : 1
eglVersionMinor : 5
eglBindAPI      : 1
eglChooseConfig : 1
Unable to create EGL surface (eglError: 0x3003)
eglSurface      : (nil)
eglQuerySurface : 0
 EGL_RENDER_BUFFER       : 
eglContext      : 0x1374030
eglQueryContext
 EGL_CONTEXT_CLIENT_TYPE : 30a0
 EGL_RENDER_BUFFER       : 3038
eglMakeCurrent  : 1
GL_VERSION      : OpenGL ES 3.0 Mesa 17.2.5
GL_RENDERER     : AMD PALM (DRM 2.50.0 / 4.13.15, LLVM 4.0.1)
eglTerminate    : 1


mem: 0x13ad4e0: Hello World!
00000000013ad4e0  48 65 6c 6c 6f 20 57 6f  72 6c 64 21 00 90 90 90  |Hello World!....|
00000000013ad4f0  90 90 90 90 90 90 90 90  90 90 90 90 90 90 90 90  |................|
00000000013ad500  90 90 90 90 90 90 90 90  90 90 90 90 90 90 90 90  |................|
```