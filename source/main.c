/*
  ps4sdk sample, use makefile
*/

#define _KERNEL
#include <kernel.h>   // sceKernel...(..)

#include <stdio.h>    // printf(..)
#include <unistd.h>   // getuid()

#include <egl.h>

/* Program Entry Point */
int main()
{

    #ifdef __PS4__
    /*#include <ps4/kernel.h> //ps4KernelCall
        if(getuid())
            ps4KernelCall(ps4KernelPrivilegeEscalate); // get root */
    #endif
    printf("uid: %u\n", getuid());
  
  
    for (int i = 0; i < 3; i++) // Waits for 3 seconds
    {
      // Sleep in microsecondsz
      sceKernelSleep(1);
    } // End for i
  
    /*EGLDisplay eglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    
    printf("eglDisplay  : %p\n", eglDisplay);
    
    // Initialize EGL for this display, returns EGL version
    EGLint eglVersionMajor, eglVersionMinor;
    int ret = 1337;

    ret = eglInitialize(eglDisplay, &eglVersionMajor, &eglVersionMinor);
  
    printf("eglInitialize: %d\n", ret);
    printf("GL_VERSION \t: %s\n", glGetString(GL_VERSION) );
    printf("GL_RENDERER\t: %s\n", glGetString(GL_RENDERER) );

    ret = eglTerminate(eglDisplay);
    printf("eglTerminate: %d\n", ret);*/
    
    egl_test();

    return 0;
}
